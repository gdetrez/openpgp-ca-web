+++
title = "OpenPGP CA on IRC"
keywords = ["irc", "community"]
author = "Heiko"
date = 2021-05-20T12:30:00+01:00
banner = "empty.jpg"
+++

<!--
SPDX-FileCopyrightText: 2019-2021 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

As of today, you can meet the OpenPGP CA team - as well as other users - 
on the OFTC IRC network, in the channel #openpgp-ca. Come hang out 
with us there, and tell us about the projects you're building on top of 
OpenPGP CA!
