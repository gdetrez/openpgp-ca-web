+++
title = "OpenPGP CA 0.10.1 released"
keywords = ["release"]
author = "Heiko"
date = 2021-05-07T10:30:00+01:00
banner = "empty.jpg"
+++

<!--
SPDX-FileCopyrightText: 2019-2021 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

Today we're happy to announce the first release of the OpenPGP CA 0.10.x 
series. The code for OpenPGP CA 0.10.1 is 
[tagged on gitlab](https://gitlab.com/openpgp-ca/openpgp-ca/-/tags/0.10.1).

# crates.io

Starting with this release, we're publishing to
[crates.io](https://crates.io]). So from now on, you can conveniently 
`cargo install openpgp-ca`.

Right now, we're publishing two crates:
* The openpgp-ca CLI tool: [crates.io/crates/openpgp-ca](https://crates.io/crates/openpgp-ca)
* The underlying library that implements the functionality of OpenPGP CA 
  (the CLI tool is a slim wrapper around this library): 
  [crates.io/crates/openpgp-ca-lib](https://crates.io/crates/openpgp-ca-lib)

Note: for the time being, there is no release of the 
[OpenPGP CA REST daemon](https://openpgp-ca.org/doc/restd/) on crates.io, 
since it depends on [rocket](https://rocket.rs/) 0.5, which is
[not yet released](https://github.com/SergioBenitez/Rocket/issues/1476).

While we're talking crates, there's also the related
[crates.io/crates/openpgp-keylist/](https://crates.io/crates/openpgp-keylist/),
which is used by openpgp-ca for exporting to
[Keylist format](https://code.firstlook.media/keylist-rfc-explainer).

<!--more-->

# Behind the scenes

After some refactoring of the codebase, the 0.10.x series paves the way for 
OpenPGP CA instances with different cryptographic backends and/or 
different data storage models.

One concrete plan is to implement support for OpenPGP CA instances based 
around [OpenPGP card](https://en.wikipedia.org/wiki/OpenPGP_card)
hardware tokens, such as [Gnuk](https://wiki.debian.org/GNUK) or YubiKey. 
With such OpenPGP CA instances, no private key material will be stored in 
the CA database. All operations that require CA private key material 
will be performed on the hardware token.

We've also discussed read-only instances that don't contain CA private 
key material, for example as part of an
[air-gapped](https://en.wikipedia.org/wiki/Air_gap_%28networking%29) 
setup, and are considering implementing support for that use case.
