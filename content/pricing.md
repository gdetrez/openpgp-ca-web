+++
title = "Pricing"
keywords = ["pricing"]
+++

# OpenPGP CA is Free Software

OpenPGP CA is Free Software (under the
[GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html)), so you are of 
course always allowed to use OpenPGP CA free of charge.

# Paid support

However, we're happy to offer consulting, support or development work. 
Support contracts are a good way for organizations to financially 
back the project.

We offer a standard package that includes one-time training and 
guidance for rolling out OpenPGP CA in your organization. If your 
organization needs more extensive support for projects around OpenPGP and 
authentication, we can specify a custom package to suit your specific needs.

Reduced rates are available for non-profit organizations.

# Contact

For inquiries, contact <heiko@schaefer.name>
(PGP [68C8B3725276BEBB3EEA0E208ACFC41124CCB82E](https://openpgp-ca.org/heiko.asc)).
